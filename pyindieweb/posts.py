import math
import os
import urllib.parse
from itertools import chain
import requests
import mf2py
import mf2util
from flask import json, jsonify, request, safe_join, abort, url_for, session
from pyindieweb import app, render_template

# THIS ONLY WORKS ON UWSGI
try:
    import uwsgi
except ModuleNotFoundError:
    app.logger.critical("This thing only supports uwsgi.")
    raise
class Pagination():
    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page + 1
        self.total_count = total_count

    @property
    def pages(self):
        return int(math.ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num

def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)


def hcardify(url):
    if urllib.parse.urlparse(url).hostname == request.host:
        return app.config["BLOGS"][request.host]["author"]["name"]
    else:
        if uwsgi.cache_exists(url, 'hcards'):
            return json.loads(uwsgi.cache_get(url, 'hcards'))
        else:
            #return list(filter(lambda i: "h-card" in i["type"], mf2py.parse(url=url)["items"]))[0]
            r = requests.get(url)
            print(url, r.url)
            page = mf2py.parse(doc=r.content, url=r.url)
            hcard = mf2util.representative_hcard(page, r.url) # or mf2util.representative_hcard(page, url + "/")
            uwsgi.cache_update(url, bytes(json.dumps(hcard), encoding="utf-8"), 64800, 'hcards')
            return hcard


def getdomain(url):
    return urllib.parse.urlparse(url).hostname

app.jinja_env.globals['url_for_other_page'] = url_for_other_page
app.jinja_env.globals['hcardify'] = hcardify
app.jinja_env.globals['getdomain'] = getdomain

def _load_post(path, filename):
    with open(safe_join(path, filename)) as f:
        return json.load(f)

def posts_dir():
    return os.path.join(app.instance_path, request.host, 'posts')

class PostGoneException(Exception):
    pass

def get_post(kind, postid):
    #with open(safe_join(posts_dir(), kind, postid + ".json")) as f:
    #    post = json.load(f)
    post = _load_post(safe_join(posts_dir(), kind), postid + ".json")
    if post.get("_deleted", False):
        raise PostGoneException()
    return post

def get_list(kind):
    if kind != "all":
        path = safe_join(posts_dir(), kind)
        try:
            files = os.listdir(path)
        except FileNotFoundError:
            abort(404, "This post category is empty.")
        posts = []
        for post in files:
            if post[-5::1] != ".json":
                # THIS IS NOT A POST
                # IF IT IS NOT JSON
                continue
            p = _load_post(path, post)
            if  (not p.get("_deleted", False)) \
            and (session.get("me") or not p["properties"].get("visibility", ["public"])[0] == "unlisted") \
            and (session.get("me") in p["properties"].get("audience", []) or not p["properties"].get("visibility", ["public"])[0] == "private"):
                posts.append(p)
    else:
        posts = list(chain.from_iterable(map(get_list, filter(lambda p: p not in ["like", "bookmark"], os.listdir(posts_dir())))))
    return sorted(posts, key=lambda p: p["properties"]["published"], reverse=True)

def list_posts(kind='all', page=1, template='list.html'):
    posts = get_list(kind)
    paginate = app.config.get("PAGINATE", 5)
    pagelist = posts[(page-1)*(paginate+1):(page*(paginate+1))-1]
    pagination = Pagination(page, paginate, len(posts))
    #print('\n\n'.join([str(p["properties"]["url"][0]) + ": " + str(p["properties"]) for p in pagelist]))
    return render_template(
        template,
        posts=pagelist,
        pagination=pagination,
        kind=kind
    ), 200, {'WWW-Authenticate': 'IndieAuth'}

@app.route('/all')
def full_feed():
    return render_template(
        'list.html',
        posts=get_list('all'),
        pagination=Pagination(0, 10, 1),
        kind='Full Feed'
    ), 200, {'WWW-Authenticate': 'IndieAuth'}
    

@app.route('/tags/<string:tag>')
@app.route('/tags/<string:tag>/page/<int:page>')
def list_tag(tag, page=1):
    postlist = get_list('all')
    posts = list(filter(lambda p: tag.lower() in map(lambda i: i.lower(), p["properties"].get("category", [])), postlist))
    paginate = app.config.get("PAGINATE", 5)
    pagelist = posts[(page-1)*paginate:(page*paginate)-1]
    pagination = Pagination(page, paginate, len(posts))
    return render_template(
        'list.html',
        posts=pagelist,
        pagination=pagination,
        kind="#{}".format(tag)
    # The header signifies that private posts may be available if proper authentication flow is done
    ), 200, {'WWW-Authenticate': 'IndieAuth'}

def serve_post_json(kind, postid):
    try:
        return jsonify(get_post(kind, postid))
    except FileNotFoundError:
        return jsonify({"error": "not_found"}), 404
    except PostGoneException:
        return jsonify({"error": "gone"}), 410

def serve_post(kind, postid):
    try:
        post = get_post(kind, postid)
    except FileNotFoundError:
        abort(404, "The {kind} with ID {postid} does not exist.".format(kind=kind, postid=postid))
    except PostGoneException:
        abort(410, "The {kind} with ID {postid} is gone, presumably forever. No additional data may be provided.".format(kind=kind, postid=postid))
    if post["properties"].get("visibility", ["public"])[0] == "unlisted":
        if not session.get("me"):
            abort(401, "Viewing this post requires authentication.")
    if post["properties"].get("visibility", ["public"])[0] == "private":
        try:
            if session["me"] not in post["properties"].get("audience", []):
                abort(403, "Viewing this post is forbidden for you. No additional authentication may help.")
        except KeyError:
            abort(401, "This post is private. Maybe authenticating will help?")
    return render_template("post.html".format(kind), post=post)

app.add_url_rule('/', view_func=list_posts, defaults={'kind': 'all', 'page': 1, 'template': 'index.html'})
app.add_url_rule('/page/<int:page>', view_func=list_posts, defaults={'kind': 'all', 'template': 'index.html'})
for i in ['note', 'post', 'photo', 'video', 'like', 'bookmark', 'repost', 'reply', 'checkin']:
    app.add_url_rule('/' + i + '/', view_func=list_posts, defaults={'kind': i, 'page': 1})
    app.add_url_rule('/' + i + '/page/<int:page>/', view_func=list_posts, defaults={'kind': i})
    app.add_url_rule('/' + i + '/<string:postid>/', view_func=serve_post, defaults={'kind': i})
    if app.debug:
        app.add_url_rule('/' + i + '/<string:postid>.json', view_func=serve_post_json, defaults={'kind': i})
