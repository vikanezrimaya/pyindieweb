import crypt
import time
import hashlib
import urllib.parse
from hmac import compare_digest
import mf2py
from flask import json, jsonify, request, abort, redirect
from pyindieweb import app, render_template


def check_password(password):
    return compare_digest(crypt.crypt(password, salt=app.config["BLOGS"][request.host]["pass"]), app.config["BLOGS"][request.host]["pass"])

@app.route('/api/indieauth', methods=['GET', 'POST'])
def indieauth():
    if request.method == "GET":
        # We probably have requested authentication!
        try:
            data = request.args
            me = data["me"]
            client_id = data["client_id"]
            redirect_uri = data["redirect_uri"]
            state = data["state"]
            response_type = data.get("response_type", "id")
            scopes = data.get("scope", "").split()
            if response_type not in ['id', 'code'] or \
               (response_type == "code" and len(scopes) == 0):
                   raise Exception
        except:
            abort(400, """The app you want to log into seems to have broken IndieAuth support.
                          Here are the parameters it sent: {}\n\n{}""".format(json.dumps(data)))
        url_me = urllib.parse.urlparse(me)
        if url_me.hostname not in map(lambda b: b.split(':')[0], app.config["BLOGS"].keys()):
            abort(403, "The <code>me</code> parameter is incorrect. Cannot authenticate as the server has no authority on the identity of the requested user.")
        h_app_page = mf2py.parse(url=client_id)
        try:
            if h_app_page["rels"]["redirect_uri"] != redirect_uri:
                abort(400, "The app's redirect_uri on homepage doesn't correspond to passed redirect_uri. pyindieblog prevented you from authenticating on this site to protect your identity and the redirect_uri endpoint.")
            else:
                verified = True
        except KeyError:
            verified = False
        try:
            h_app = list(filter(lambda i: any((True for t in ["h-app", "h-x-app"] if t in i["type"])), h_app_page))[0]
        except:
            h_app = None
        # If we here, means the data **seems** to be well-formed. We can present this to user!
        return render_template('indieauth.html',
            me=me, client_id=client_id, redirect_uri=redirect_uri, state=state, response_type=response_type, scopes=scopes,
            verified=verified, h_app=h_app
        )
    elif request.method == "POST":
        m = hashlib.sha256()
        data = request.form["code"].split('-')
        digest = data[0]
        scopes = data[1].split('^')[1].split('+')
        valid_until = int(data[2])
        if time.time() > valid_until:
            return jsonify({"error": "forbidden", "error_description": "token has expired"}), 403
        if scopes == [""]:
            scopes = []

        data = {"client_id": request.form["client_id"], "redirect_uri": request.form["redirect_uri"], "scope": scopes, "valid_until": valid_until}
        m.update(bytes(json.dumps(data), encoding="utf-8"))
        m.update(app.config["SECRET_KEY"])
        response = {'me': request.scheme + "://" + request.host + "/"}
        response.update({"scope": ' '.join(scopes)})
        if m.hexdigest() == digest:
            if request.headers["Accept"] == "application/json":
                return jsonify(response)
            else:
                return urllib.parse.urlencode(response)
        else:
            return jsonify({"error": "forbidden", "input": request.form, "digest": m.hexdigest(), "parsed": data}), 403

@app.route('/api/indieauth/confirm', methods=['POST'])
def confirm_auth():
    #abort(501, "Under Construction")
    #return jsonify({
    #    "me": request.form["me"],
    #    "client_id": request.form["client_id"],
    #    "redirect_uri": request.form["redirect_uri"],
    #    "response_type": request.form["response_type"],
    #    "state": request.form["state"],
    #    "scope": request.form.getlist("scope"),
    #    "password": request.form["password"]
    #})
    if check_password(request.form["password"]):
        m = hashlib.sha256()
        valid_until = int(time.time()) + 300
        data = {"client_id": request.form["client_id"], "redirect_uri": request.form["redirect_uri"], "scope": request.form.getlist("scope"), "valid_until": valid_until}
        m.update(bytes(json.dumps(data), encoding="utf-8"))
        m.update(app.config["SECRET_KEY"])
        return_value = {"code": m.hexdigest()+"-scope^"+'+'.join(request.form.getlist("scope"))+"-"+str(valid_until)}
        if "state" in request.form:
            return_value.update({"state": request.form["state"]})
        app.logger.critical(json.dumps(data))
        app.logger.critical(return_value)
        return redirect(request.form["redirect_uri"] + "?" + urllib.parse.urlencode(return_value))
