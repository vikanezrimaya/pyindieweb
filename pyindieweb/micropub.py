import dateutil.parser
import os
import random
import re
import threading
import time
import shutil
import subprocess
import string
import urllib.parse
from datetime import datetime, timezone, timedelta
import mf2py
import mf2util
import requests
from werkzeug.datastructures import FileStorage
from flask import json, jsonify, request, copy_current_request_context
import pyindieweb.webmentions
from pyindieweb import app
from pyindieweb.tokens import token_valid
from pyindieweb.posts import hcardify

extensions = {
    'image/gif': 'gif',
    'image/png': 'png',
    'image/jpg': 'jpg'
}
hashtags = re.compile(r"((?<=\s)|^)#{1}(\w+)")
mentionregex = re.compile('(^|(?<=\\s))@{1}([^\\s#<>[\\]|{}]+)') 


def get_post_type(post, allow_reply_type=True):
    if "name" in post["properties"]:
        # This is an article.
        post_type = "post"
    elif "like-of" in post["properties"]:
        post_type = "like"
    elif "bookmark-of" in post["properties"]:
        post_type = "bookmark"
    elif "repost-of" in post["properties"]:
        post_type = "repost"
    else:
        post_type = "note"
    if "photo" in post["properties"] and len(post["properties"].get("photo", [])) > 0:
        post_type = "photo"
    if "video" in post["properties"] and len(post["properties"].get("photo", [])) > 0:
        post_type = "video"
    if allow_reply_type and "in-reply-to" in post["properties"]:
        post_type = "reply"
    return post_type
app.jinja_env.globals['post_kind'] = lambda post: get_post_type(post, allow_reply_type=False)

def now_iso():
    return datetime.now(tz=timezone(timedelta(hours=3))).isoformat(timespec="seconds")

def get_location(data, date=None):
    """Calculate a post's location."""
    posted_on = data.get("published") or date or now_iso()
    post_type = get_post_type(data)
    if data.get("name"):
        filename = data["name"].lower().replace(" ", "-").replace(".", "-").replace("/", "-").replace("#", "").replace(":", "-").replace("?", "-").replace("--", "-")
    else:
        filename = str(int(dateutil.parser.parse(date).timestamp()))
    return "{category}/{name}".format(category=post_type, name=filename)

def mentions(text):
    return mentionregex.findall(text)

def process_photos(photo):
    if isinstance(photo, FileStorage):
        mimetype = photo.mimetype
        filename = ''.join(random.choices(string.ascii_lowercase + string.digits, k=16))
        ext = extensions.get(mimetype)
        with open(os.path.join(app.instance_path, request.host, 'uploads', filename + "." + ext), 'wb') as f:
            photo.save(f)
        return request.scheme + "://" + os.path.join(request.host, 'uploads', filename + "." + ext)
    elif isinstance(photo, str):
        url = urllib.parse.urlparse(photo)
        if url.domain not in app.config["BLOGS"]:
            filename = ''.join(random.choices(string.ascii_lowercase + string.digits, k=16))
            ext = url.path.split('.')[-1]
            with requests.get(photo, stream=True) as r:
                with open(os.path.join(app.instance_path, request.host, fn + "." + ext), 'wb') as f:
                    # This makes copying files EXTREMELY fast
                    shutil.copyfileobj(r.raw, f)
            return request.scheme + "://" + os.path.join(request.host, 'uploads', fn + "." + ext)
        else:
            return photo

def parse_post(url):
    app.logger.info("Parsing metadata from %s", url)
    urlparsed = urllib.parse.urlparse(url)
    if urlparsed.hostname == "twitter.com":
        app.logger.info("Tweet detected.")
        tweetid = url.split("/")[-1]
        url = "https://granary.io/twitter/@me/@all/@app/{postid}?format=html&access_token_key={key}&access_token_secret={secret}".format(postid=tweetid, key=app.config["TOKENS"]["twitter"]["key"], secret=app.config["TOKENS"]["twitter"]["secret"])
    app.logger.info("Trying to parse h-entry from url...")
    post = mf2py.parse(url=url)
    app.logger.critical(post)
    post = list(filter(lambda e: e["type"] == ["h-entry"], post["items"]))
    app.logger.critical(post)
    post = post[0]
    return post

def create(data, date=None, location=None):
    webmentions = set()
    # XXX should this reside in ["properties"]?
    blog = data["properties"].get("mp-destination", request.host)
    date = data["properties"].setdefault("published", [date or now_iso()])[0]
    location = location or get_location(data, date)
    url = data["properties"].setdefault("url", [request.scheme + "://" + blog + "/" + location])[0]
    data["properties"]["category"] = list(set(data["properties"].setdefault("category", [])))
    filename = os.path.join(app.instance_path, blog, 'posts', location + ".json")
    if os.path.exists(filename):
        return jsonify({"error": "invalid_request", "error_description": "Cannot overwrite posts with create()."}), 400
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    with open(filename, 'w') as f:
        json.dump(data, f)
    os.chdir(os.path.join(app.instance_path, blog))
    # Adding person mentions
    try:
        # Progressive enhancement
        content = ""
        html = False
        content = data["properties"].get("content")[0]
        content = content["html"]
        html = True
    except (KeyError, IndexError, TypeError):
        pass
    try:
        with open(os.path.join(app.instance_path, blog, 'blogroll.json'), 'r') as f:
            # Reading our blogroll
            blogroll = json.load(f)
    except FileNotFoundError:
        blogroll = {}
    
    def process_mention(match):
        mention = match[0]
        if mention[0] == "#":
            # persontag
            mention = mention[2:]
            string = '<a href="{}" class="u-category">{}</a>'
        else:
            mention = mention[1:]
            string = '<a href="{}">{}</a>'

        if mention in blogroll:
            url = blogroll[mention]["url"]
            name = blogroll[mention]["name"]
            webmentions.add(url)
        elif "." in mention:
            # This block of code makes a (reasonable) dangerous assumption that everyone I'm gonna @mention is using https.
            # This assumption is reasonable AND dangerous.
            # Caveat emptor.
            hcard = hcardify("http://" + mention)
            #page = mf2py.parse(url=url)
            #hcard = mf2util.representative_hcard(page, url) or mf2util.representative_hcard(page, url + "/")
            #app.logger.critical(hcard)
            try:
                name = hcard.get("properties", {}).get("name", [])[0]
                webmentions.add(hcard.get("properties", {}).get("url", [])[0])
            except Exception as e:
                app.logger.exception(e)
                return match[0]
        else:
            url = "https://twitter.com/{}".format(mention)
            name = match[0]

        return "<a href=\"{}\">{}</a>".format(url, name)
    try:
        content = mentionregex.sub(process_mention, content)
        if html:
            data["properties"]["content"] = [{"html": content}]
        else:
            data["properties"]["content"] = [content]
    except Exception as e:
        app.logger.exception(e)
    def process_hashtag():
        string = '<a href="{scheme}://{blog}/tags/{tag}" class="u-category">#{tag}</a>'.format(scheme=request.scheme, blog=blog, tag="{}")
        def _hashtag(match):
            tag = match[0][1:]
            data["properties"].setdefault("category", []).append(tag)
            return string.format(tag, tag)
        return _hashtag
    try:
        content = hashtags.sub(process_hashtag(), content)
        if html:
            data["properties"]["content"] = [{"html": content}]
        else:
            data["properties"]["content"] = [content]
    except Exception as e:
        app.logger.exception(e)

    for i in range(len(data["properties"].get('in-reply-to', []))):
        u = data["properties"]["in-reply-to"][i]
        webmentions.add(u)
        data["properties"]["in-reply-to"][i] = parse_post(u)
        data["properties"]["in-reply-to"][i]["properties"]["archived"] = [now_iso()]
    for u in data["properties"].get('like-of', []):
        webmentions.add(u)
    for u in data["properties"].get('bookmark-of', []):
        webmentions.add(u)
    for u in data["properties"].get('repost-of', []):
        webmentions.add(u)
    for u in data["properties"].get('follow-of', []):
        webmentions.add(u)
    webmention_responses = dict()

    # Syndicate first (easier deduplication for webmentioned people)
    for target in data["properties"].get("mp-syndicate-to", []):
        app.logger.info("Sending webmention for %s", target)
        #webmention = requests.post("https://telegraph.p3k.io/webmention", data={
        #    "token": app.config["TOKENS"]["telegraph"],
        #    "source": url,
        #    "target": target
        #})
        try:
            if target == "https://brid.gy/publish/twitter":
                webmention = pyindieweb.webmentions.send(url, target, {"bridgy_omit_link": "maybe"})
            else:
                webmention = pyindieweb.webmentions.send(url, target)
        except Exception as e:
            app.logger.exception(e)
            continue
        if webmention.ok:
            app.logger.info("Sending webmention for {}: OK".format(target))
            if target in ["https://brid.gy/publish/twitter", "https://news.indieweb.org/en"]:
                syndicated_url = webmention.json()["url"]
                data["properties"]['mp-syndicate-to'].remove(target)
                data["properties"].setdefault('syndication', []).append(syndicated_url)
        else:
            app.logger.warning("Sending webmention for {}: FAIL".format(target))
        webmention_responses.update({target: webmention.json()})
    # Finalize writing file again to add syndication links!
    with open(filename, 'w') as f:
        json.dump(data, f)
    #subprocess.run(["git", "add", os.path.join('posts', location + ".json")])
    #subprocess.run(["git", "commit", "-m", "Automatic commit from Micropub"])
    # Pinging hub that we made a new post!
    try:
        requests.post("https://switchboard.p3k.io/", data=bytes(urllib.parse.urlencode([
            ('hub.mode', 'publish'),
            ('hub.url[]', 'https://'+request.host+'/'),
            ('hub.url[]', 'https://'+request.host+'/'+os.path.dirname(location)+'/')] + \
            list(map(lambda t: ('hub.url[]', 'https://'+request.host+'/tag/'+t+'/'), data["properties"]["category"]))
        ), encoding='utf-8'))
    except Exception as e:
        app.logger.exception(e)
    try:
        if app.config["BLOGS"][request.host]["author"]["contact"].get("fediverse_nick"):
            pyindieweb.webmentions.send(url, 'https://fed.brid.gy')
    except Exception as e:
        app.logger.exception(e)

    #for u in data["properties"].get('in-reply-to', []):
    #    webmentions.add(u)
    for target in webmentions:
        app.logger.info("Sending webmention for %s", target)
        #webmention = requests.post("https://telegraph.p3k.io/webmention", data={
        #    "token": app.config["TOKENS"]["telegraph"],
        #    "source": url,
        #    "target": target
        #})
        try:
            if "niu.moe" in target: # XXX hardcoding stuff, yay
                continue
            else:
                webmention = pyindieweb.webmentions.send(url, target)
        except Exception as e:
            app.logger.exception(e)
            continue
        if webmention.ok:
            app.logger.info("Sending webmention for {}: OK".format(target))
        else:
            app.logger.warning("Sending webmention for {}: FAIL".format(target))
        webmention_responses.update({target: webmention.json()})
    return jsonify({"status": "posted", "webmentions": webmention_responses}), 201, {"Location": url}
    

def create_async(data):
    """Asynchronously launch the create() method."""
    posted_on = data.get("published") or now_iso()
    blog = data.get("destination", request.host)
    location = get_location(data, posted_on)
    if os.path.exists(os.path.join(app.instance_path, blog, 'posts', location + ".json")):
        return jsonify({"error": "invalid_request", "error_description": "Cannot overwrite posts with create()."}), 400
    thread = threading.Thread(target=copy_current_request_context(create), args=(data, posted_on, location))
    thread.start()
    return jsonify({"status": "accepted"}), 202, {"Location": request.scheme + "://" + blog + "/" + location}

def update(data):
    pass

def update_async(data):
    pass

def normalize_form(form):
    data = {"type": ["h-{}".format(form.get("h"))], "properties": {}}
    for entry in form:
        if entry == "h":
            continue
        elif "[]" in entry:
            name = entry.strip('[]')
        else:
            name = entry
        # We need to preserve these values as lists basing on Micropub specification
        data["properties"].update({name: form.getlist(name) + form.getlist(name + "[]")})
    return data

@app.route('/api/micropub', methods=['GET', 'POST'])
def micropub():
    print(request.host)
    if request.method == "POST":
        # Processing both JSON and form-encoded data
        data = request.get_json()
        if data is None:
            data = normalize_form(request.form)
            if "photo" in data or "photo" in request.files or "photo[]" in request.files:
                data.update({"photo": data.get("photo", []) + request.files.getlist("photo") + request.files.getlist("photo[]")})
        # Extracting token
        token = request.headers.get("Authorization", "Bearer ").split(' ')[1] or data.get("access_token", "")
        # If we're not in production, accept requests without a token
        # YES, THIS IS INSECURE AS @#$*
        # YOU HAVE BEEN @#$*ING WARNED
        if not app.debug:
            # If token is empty
            if token == "":
                return jsonify({"error": "authorization_required"}), 401
            # Returning validity and scope
            try:
                valid, scope = token_valid(token)
                print(valid, scope)
                if not valid:
                    # 403 Forbidden if token is denied permission to write
                    return jsonify({"error": "insufficient_scope", "scope": "create"}), 401
            except KeyError as e:
                app.logger.exception(e)
                return jsonify({"error": "forbidden"}), 403
        try:
            del data["access_token"]
        except KeyError:
            pass
        # If we're here, the request is fully authorized. We can process it.
        # Micropub specification declares h=entry as a required parameter for posts.
        # If the post is at least a valid like, bookmark, note or a media-post, it can be safely processed
        entry = data.get("type")[0]
        valid_h_entry = any(True for x in ["like-of", "bookmark-of", "repost-of", "content", "photo", "video"] if x in data["properties"])
        print("type =", entry, "h-entry =", valid_h_entry)
        if entry in ["h-entry"] and valid_h_entry:
            if app.config.get("SYNC", False):
                # Processing data synchronously
                return create(data)
            else:
                # Creating a new thread to post data
                return create_async(data)
        else:
            return jsonify({"error": "bad request"}), 400
    elif request.method == "GET":
        if request.args.get("q") == "config":
            return jsonify({
                "syndicate-to": list(map(
                    lambda i: {"uid": app.config["SYNDICATE"][i], "name": i},
                    app.config["SYNDICATE"].keys()
                )),
                "media-endpoint": request.scheme + "://" + request.host + "/api/micropub/media",
                # Since we render posts from Micropub requests,
                # we can handle every single post in existence
                # as long as it can be represented by mf2 JSON
                #"post-types": list(map(lambda i: {"type": i[0], "name": i[1]}, {
                #    "note": "Note",
                #    "article": "Article",
                #    "photo": "Photo",
                #    "video": "Video",
                #    "reply": "Reply",
                #    "like": "Like"
                #}.items())),
                "destination": list(map(lambda blog: {"uid": request.scheme + "://" + blog, "name": blog}, app.config.get("BLOGS").keys()))
            }), 200
        elif request.args.get("q") == "syndicate-to":
            return jsonify({"syndicate-to": list(map(lambda i: {"uid": app.config["SYNDICATE"][i], "name": i}, app.config["SYNDICATE"]))}), 200
        else:
            return jsonify({"error": "invalid_request"}), 400

@app.route('/api/micropub/media', methods=['GET', 'POST'])
def media():
    token = request.headers.get("Authorization", "Bearer ").split(' ')[1] or data.get("access_token", "")
    if not app.debug:
        # If token is empty
        if token == "":
            return jsonify({"error": "authorization_required"}), 401
        try:
            # Returning validity and scope
            valid, scope = token_valid(token, "media")
            if "media" not in scope:
                # 403 Forbidden if token is denied permission to write
                return jsonify({"error": "insufficient_scope", "scope": "media"}), 401
            elif not valid:
                return jsonify({"error": "forbidden"}), 403
        except KeyError:
            return jsonify({"error": "unauthorized"}), 401
    # Request authorized... processing
    if request.method == "POST":
        filename = process_photos(request.files.get("file"))
        return "", 201, {"Location": filename}
    else:
        return jsonify({"error": "invalid_request"}), 400
