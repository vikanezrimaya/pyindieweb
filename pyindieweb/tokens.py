import urllib
import requests
from flask import request
from pyindieweb import app


def token_valid(token, scope=["create"]):
    """Check the validity of the token."""
    #try:
    response = requests.get(app.config["BLOGS"][request.host]["endpoints"]["tokens"], headers={
        'Authorization': "Bearer " + token,
        'Accept': 'application/json'
    }).json()
    print(response)
    return any((True for s in scope if s in response["scope"].split())) and urllib.parse.urlparse(response["me"]).hostname in list(app.config["BLOGS"].keys()), response["scope"].split()

@app.route('/api/indieauth/token', methods=["GET", "POST"])
def token_endpoint():
    abort(501, "Under construction")
