import urllib.parse
import requests
import mf2py
from flask import request
from pyindieweb import app

def send(source, target, params=None):
    try:
        target_page = mf2py.parse(url=target)
        endpoint = target_page["rels"].get("webmention", target_page["rels"].get("http://webmention.org/"))[0]
        if endpoint is None:
            raise KeyError
        data = {'source': source, 'target': target}
        if params:
            data.update(params)
        app.logger.info("Sending a webmention to {} with parameters: ".format(endpoint) + str(data))
        return requests.post(endpoint, data=data)
    except:
        raise
